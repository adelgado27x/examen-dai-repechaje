var mysql = require('mysql');
//Atencion controller 
module.exports = {
    //
    //Listar atenciones
    getAtencion: function (req, res, next) {
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var atenciones = null;

        db.query('SELECT atencion.id_aten, DATE_FORMAT(atencion.fec_aten,"%Y-%m-%d %H:%i")AS fec, atencion.estado_aten, atencion.pac_id, medico.name_med,medico.esp_med ' +
            'FROM atencion ' +
            'INNER JOIN medico ' +
            'ON atencion.med_id = medico.id_med', function (err, rows, fields) {
                if (err) throw err;

                atenciones = rows;
                db.end();

                res.render('atencion/atencionList', { atenciones: atenciones });
            });
    },
    //
    //renderizar pagina menu registro atencion
    getNuevaAtencion: function (req, res, next) {
        var id = req.params.id;

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();
        //Completar la datalist
        var idPac = null;
        db.query('SELECT id_pac FROM paciente', function (err, rows, fields) {
            if (err) throw err;

            idPac = rows;
             res.render('atencion/nuevaAtencion', { idPac: idPac });
        });       
    },
    //
    //renderizar pagina de registro a pediatria
    getAtencionPed: function (req, res, next) {
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var medicoPed = null;
        db.query('SELECT name_med, id_med FROM medico WHERE esp_med = "Pediatria"', function (err, rows, fields) {
            if (err) throw err;

            medicoPed = rows;
            db.end();

            res.render('atencion/atencionPed', { medicoPed: medicoPed });
        });
    },
    //
    //renderizar pagina de registro a kinesiologia
    getAtencionKine: function (req, res, next) {
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var medicoKine = null;
        db.query('SELECT name_med, id_med FROM medico WHERE esp_med = "Kinesiologo"', function (err, rows, fields) {
            if (err) throw err;

            medicoKine = rows;
            db.end();

            res.render('atencion/atencionKine', { medicoKine: medicoKine });
        });
    },
    //
    //renderizar pagina de registro a Neurocirugia
    getAtencionNeu: function (req, res, next) {
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var medicoNeu = null;
        db.query('SELECT name_med, id_med FROM medico WHERE esp_med = "Neurocirujano"', function (err, rows, fields) {
            if (err) throw err;

            medicoNeu = rows;
            db.end();

            res.render('atencion/atencionNeu', { medicoNeu: medicoNeu });
        });
    },
    //
    //renderizar pagina de registro a Nutricionista
    getAtencionNutri: function (req, res, next) {
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var medicoNutri = null;
        db.query('SELECT name_med, id_med FROM medico WHERE esp_med = "Nutricionista"', function (err, rows, fields) {
            if (err) throw err;

            medicoNutri = rows;
            db.end();

            res.render('atencion/atencionNutri', { medicoNutri: medicoNutri });
        });
    },
    //
    //obtener datos, despues seran enviados a la BD por query
    postCrearAtencion: function (req, res, next) {

        var atencion = {
            fec_aten: req.body.fecha,
            estado_aten: 'Agendado',
            pac_id: req.body.idPac,
            med_id: req.body.idMed
        }
        //verifica los datos ingresados
        console.log(atencion);

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query('INSERT INTO atencion SET ?', atencion, function (err, rows, fields) {
            if (err)throw err;
            
            db.end();
        });
        res.render('index',{infoAdd : 'Atencion Registrada Correctamente'});
    },
    //
    //Obtener datos de la atencion ante previa modificacion
    //y renderizarlo junto a los datos obtenidos hacia la pagina de modificacion
    getModificarAten: function (req, res, next) {
        var id = req.params.id;
        console.log(id);

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var atencion = null;

        db.query('SELECT DATE_FORMAT(atencion.fec_aten,"%Y-%m-%dT%H:%i")AS fec,estado_aten FROM atencion WHERE id_aten = ?', id, function (err, rows, fields) {
            if (err) throw err;

            atencion = rows;
            db.end();

            res.render('atencion/modificarAten', { atencion: atencion });
        });
    },
    //
    //Modificar atencion
    postModificarAten: function (req, res, next) {
        var atencion = {
            fec_aten: req.body.fecha,
            estado_aten: req.body.estado,
        }

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query('UPDATE atencion SET ? WHERE ? ', [atencion, { id_aten: req.body.id }], function (err, rows, fields) {
            if (err) throw err;
            db.end();
        });
        res.render('index',{infoMod : 'Atencion Modificada Exitosamente'});
    }
}
