var mysql = require('mysql');
//Medico controller 
module.exports = {
    //
    //Listar medicos
    getMedicos:function(req,res,next){
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var medicos = null;

        db.query('SELECT * FROM medico',function(err, rows, fields){
            if(err) throw err;

            medicos = rows;
            db.end();

            res.render('medicos/medicosList',{medicos : medicos});
        });   
    },
    //
    //renderizar pagina nuevo medico
    getNuevoMedico : function(req,res,next){
        res.render('medicos/nuevoMedico')
    },
    //
    //obtener datos, despues seran enviados a la BD por query
    postNuevoMedico : function(req,res,next){

        var medico = {
         id_med : req.body.id,
         name_med : req.body.name,
         fec_contra : req.body.contra,
         esp_med : req.body.esp,
         valor_med: req.body.valor
        }
        //verifica los datos ingresados
        console.log(medico);

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query('INSERT INTO medico SET ?',medico,function(err,rows,fields){
            if(err)throw err;

            db.end();
        });
        res.render('index',{infoAdd : 'Medico registrado correctamente'});
    },
    //
    //Eliminar paciente
    eliminarMedico : function(req,res,next){
        var id = req.params.id

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query("DELETE FROM medico WHERE id_med = ?", id, function(err,rows,fields){
            if(err)throw err;

            db.end();
        });
        res.render('index',{infoDel : 'Medico Eliminado Exitosamente'});
    },
    //
    //Obtener datos del medico ante previa modificacion
    //y renderizarlo junto a los datos obtenidos hacia la pagina de modificacion
    getModificarMedico : function(req, res, next){
        var id = req.params.id;
        console.log(id);

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var medico = null;

        db.query('SELECT * FROM medico WHERE id_med = ?', id,function(err,rows,fields){
            if(err) throw err;

            medico = rows;
            db.end();

            res.render('medicos/modificarMed',{medico : medico});
        });
    },
    //
    //Modificar paciente
    postModificarMedico : function(req, res, next){
        var medico = {
         id_med : req.body.id,
         name_med : req.body.name,
         fec_contra : req.body.contra,
         esp_med : req.body.esp,
         valor_med: req.body.valor
        };

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query('UPDATE medico SET ? WHERE ? ',[medico, {id_med : req.body.id}], function(err,rows,fields){
            if(err)throw err;
            db.end();

        });
        res.render('index',{infoMod : 'Medico Modificado Exitosamente'});
    }
}