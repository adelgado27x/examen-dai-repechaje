
var mysql = require('mysql');
//
//paciente controller 
module.exports = {
    //
    //funcione del cotrolador, conexion de BD
    getPacientes:function(req,res,next){
        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var pacientes = null;

        db.query('SELECT * FROM paciente',function(err, rows, fields){
            if(err) throw err;

            pacientes = rows;
            db.end();

            res.render('pacientes',{pacientes : pacientes});
        });   
    },
    //
    //renderizar pagina nuevo paciente
    getNuevoPaciente : function(req,res,next){
        res.render('nuevoPaciente')
    },
    postNuevoPaciente : function(req,res,next){

        var paciente = {
         id_pac : req.body.id,
         name_pac : req.body.name,
         fec_pac : req.body.birth,
         sexo_pac : req.body.sex,
         dir_pac : req.body.dir,
         fono_pac: req.body.fono
        }
        //verifica los datos ingresados
        console.log(paciente);

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query('INSERT INTO paciente SET ?',paciente,function(err,rows,fields){
            if(err)throw err;

            db.end();
        });
        res.render('index',{infoAdd : 'Paciente registrado correctamente'});
    },
    //
    //Eliminar paciente
    eliminarPaciente : function(req,res,next){
        var id = req.params.id

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query("DELETE FROM paciente WHERE id_pac = ?", id, function(err,rows,fields){
            if(err)throw err;

            db.end();
        });
        res.render('index',{infoDel : 'Paciente Eliminado Exitosamente'});
    },
    //
    //Obtener datos del paciente ante previa modificacion
    //y renderizarlo hacia la pagina de modificacion
    getModificarPaciente : function(req, res, next){
        var id = req.params.id;
        console.log(id);

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        var paciente = null;

        db.query('SELECT * FROM paciente WHERE id_pac = ?', id,function(err,rows,fields){
            if(err) throw err;

            paciente = rows;
            db.end();

            res.render('modificarPac',{paciente : paciente});
        });
    },
    //
    //Modificar paciente
    postModificarPaciente : function(req, res, next){
        var paciente = {
         id_pac : req.body.id,
         name_pac : req.body.name,
         fec_pac : req.body.birth,
         sexo_pac : req.body.sex,
         dir_pac : req.body.dir,
         fono_pac: req.body.fono
        };

        var config = require('.././model/config');

        var db = mysql.createConnection(config);
        db.connect();

        db.query('UPDATE paciente SET ? WHERE ? ',[paciente, {id_pac : req.body.id}], function(err,rows,fields){
            if(err)throw err;
            db.end();

        });
        res.render('index',{infoMod : 'Paciente Modificacdo Exitosamente'});
    }
}
