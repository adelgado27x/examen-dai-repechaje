-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-08-2017 a las 01:25:55
-- Versión del servidor: 5.7.14
-- Versión de PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `exmedico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atencion`
--

CREATE TABLE `atencion` (
  `id_aten` int(5) NOT NULL,
  `fec_aten` datetime NOT NULL,
  `estado_aten` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `pac_id` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `med_id` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `atencion`
--

INSERT INTO `atencion` (`id_aten`, `fec_aten`, `estado_aten`, `pac_id`, `med_id`) VALUES
(1, '2017-07-19 14:30:00', 'agendada', '14.256.652-1', '12.531.263-4'),
(3, '2017-07-14 15:15:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(4, '2017-07-15 06:05:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(5, '2017-07-15 06:05:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(6, '2017-07-15 06:05:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(7, '2017-07-15 00:32:00', 'Agendado', '12.256.321-2', '16.250.125-k'),
(8, '2017-07-19 03:00:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(9, '2017-07-28 05:06:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(10, '2017-07-07 06:00:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(11, '2017-07-21 08:59:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(12, '2017-07-21 08:59:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(13, '2017-07-18 06:05:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(14, '2017-07-14 06:05:00', 'Agendado', '10.135.225-5', '16.250.125-k'),
(15, '2017-07-20 09:06:00', 'Agendado', '14.256.652-1', '16.250.125-k'),
(16, '2017-07-20 05:09:00', 'Agendado', '12.256.321-2', '16.250.125-k'),
(17, '2017-09-10 09:50:00', 'Agendado', '10.135.225-5', '12.263.254-4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico`
--

CREATE TABLE `medico` (
  `id_med` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `name_med` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `fec_contra` date NOT NULL,
  `esp_med` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `valor_med` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medico`
--

INSERT INTO `medico` (`id_med`, `name_med`, `fec_contra`, `esp_med`, `valor_med`) VALUES
('12.256.321-1', 'Ricardo Ahumada A', '2001-06-20', 'Neurocirujano', 29000),
('12.263.254-4', 'Jocelyn Almendra Cansino', '2005-06-20', 'Neurocirujano', 30000),
('12.531.263-4', 'Alberto Gutirres Bravo', '2005-02-14', 'Kinesiologo', 20000),
('13.241.125-2', 'Andres Vasquez Barros', '2001-09-17', 'Neurocirujano', 50000),
('14.256.123-4', 'Marlon Bustamante Farias', '2012-03-05', 'Pediatria', 25000),
('15.256.125-7', 'Rodrigo Cabezas', '2004-02-15', 'Nutricionista', 50000),
('15.263.212-5', 'Marlen Tapia', '2001-08-05', 'Pediatria', 50000),
('16.213.465-7', 'Francisca Hierro Rojas', '2013-11-18', 'Kinesiologo', 20500),
('16.250.125-k', 'Carlos Montecino Blanco', '2010-06-02', 'Pediatria', 15000),
('16.263.236-2', 'Gabriel Morales Tronco', '2015-05-01', 'Nutricionista', 20000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `id_pac` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `name_pac` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `fec_pac` date DEFAULT NULL,
  `sexo_pac` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dir_pac` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fono_pac` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id_pac`, `name_pac`, `fec_pac`, `sexo_pac`, `dir_pac`, `fono_pac`) VALUES
('10.135.225-5', 'Juan Carlos Valdes', '1964-05-16', 'H', 'Calle Uno #4562', '+569 5321 1234'),
('12.256.321-2', 'Gladys Herrera', '1972-03-05', 'M', 'Calle Doce #1478', '+562 5123 5321'),
('14.256.652-1', 'Rosa Albornoz Perez', '1970-02-15', 'H', 'Calle Treinta #8695', '+562 5463 7896'),
('17.563.214-1', 'Juan Perez Salvo', '2017-01-01', 'H', 'Calle uno #1234', '+569 5321 1235'),
('18.263.251-1', 'Marcelo Carmelo', '1993-02-06', 'H', 'Calle Cincuenta #5317', '+569 5324 1539'),
('18.562.135-2', 'Javier Morales Castro', '1993-04-17', 'H', 'Calle Cincuenta #2456', '+569 4563 7896'),
('18.563.512-1', 'Pedro Jara', '2017-12-31', 'H', 'Calle uno #1234', '+569 5321 1235'),
('19.256.125-2', 'Maria Fernandez Mar', '1998-11-12', 'M', 'Calle Veinte #1346', '+562 5631 4587'),
('20.125.364-1', 'Benjamin Escarlata Escarlata', '1999-08-19', 'H', 'Calle Diez #4625', '+569 4235 7892');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `atencion`
--
ALTER TABLE `atencion`
  ADD PRIMARY KEY (`id_aten`),
  ADD UNIQUE KEY `id_aten` (`id_aten`),
  ADD KEY `pac_id` (`pac_id`,`med_id`),
  ADD KEY `med_id` (`med_id`);

--
-- Indices de la tabla `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`id_med`),
  ADD UNIQUE KEY `id_med` (`id_med`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id_pac`),
  ADD UNIQUE KEY `id_pac` (`id_pac`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `atencion`
--
ALTER TABLE `atencion`
  MODIFY `id_aten` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `atencion`
--
ALTER TABLE `atencion`
  ADD CONSTRAINT `atencion_ibfk_1` FOREIGN KEY (`med_id`) REFERENCES `medico` (`id_med`) ON UPDATE CASCADE,
  ADD CONSTRAINT `atencion_ibfk_2` FOREIGN KEY (`pac_id`) REFERENCES `paciente` (`id_pac`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
