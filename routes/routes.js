var express = require('express');
var router = express.Router();

var controllers = require('.././controllers');

/* GET home page. */
router.get('/', controllers.homecontroller.index);
//
//rutas para pacientes
router.get('/pacientes',controllers.pacienteController.getPacientes);

router.get('/nuevoPaciente',controllers.pacienteController.getNuevoPaciente);

router.post('/crearPaciente',controllers.pacienteController.postNuevoPaciente);

router.get('/eliminarPaciente/:id',controllers.pacienteController.eliminarPaciente);

router.get('/modificarPac/:id',controllers.pacienteController.getModificarPaciente);

router.post('/modificandoPac',controllers.pacienteController.postModificarPaciente);
//
//Rutas para medico
router.get('/medicos/medicosList',controllers.medicoController.getMedicos);

router.get('/medicos/nuevoMedico',controllers.medicoController.getNuevoMedico);

router.post('/medicos/crearMedico',controllers.medicoController.postNuevoMedico);

router.get('/medicos/eliminarMedico/:id',controllers.medicoController.eliminarMedico);

router.get('/medicos/modificarMed/:id',controllers.medicoController.getModificarMedico);

router.post('/medicos/modificandoMed',controllers.medicoController.postModificarMedico);
//
//Rutas para atencion
router.get('/atencion/atencionList',controllers.atencionController.getAtencion);

router.get('/atencion/nuevaAtencion',controllers.atencionController.getNuevaAtencion);

router.get('/atencion/atencionPed/',controllers.atencionController.getAtencionPed);

router.get('/atencion/atencionKine/',controllers.atencionController.getAtencionKine);

router.get('/atencion/atencionNeu/',controllers.atencionController.getAtencionNeu);

router.get('/atencion/atencionNutri/',controllers.atencionController.getAtencionNutri);

router.post('/atencion/crearAtencion',controllers.atencionController.postCrearAtencion);

router.get('/atencion/modificarAten/:id',controllers.atencionController.getModificarAten);

router.post('/atencion/modificandoAten',controllers.atencionController.postModificarAten);
module.exports = router;
